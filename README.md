简介：
    此项目集成了mysql + mybatis-plus ，使用mybatis-plus的自动填充功能实现通用字段的赋值，业务代码无需考虑，适用于小型快速项目开发，可快速开发出业务代码来。

一、数据库通用字段
1、id为主键 bigint类型 自增id
2、create_time为创建时间 timestamp类型（业务不用管它，代码里拦截器自动设置）
3、update_time为更新时间 timestamp类型（业务不用管它，代码里拦截器自动设置）
4、create_by为创建人id bigint类型 （业务不用管它，代码里拦截器解析token自动设置）
5、update_by为更新人id bigint类型 （业务不用管它，代码里拦截器解析token自动设置）
6、is_delete为逻辑删除标识  在数据库建表时设置默认值0即可
二、前端调用需要传token，key就为token，可在此次做更改

![img_1.png](img_1.png)

![img_2.png](img_2.png)

三、消息异常拦截，可在如下地方更改

![img_3.png](img_3.png)

四、拦截器放行地址
    可在此次新增

![img_4.png](img_4.png)

五、关于实体类

1、通用字段已封装在这里面
     ![img_5.png](img_5.png)

2、实体类集成改类即可

![img_8.png](img_8.png)

3、如果有联查或者传参在实体类之外的字段的话，可继承与实体类

六、新增了邮件发送功能

![img_9.png](img_9.png)

![img_10.png](img_10.png)

七、新增了minio功能

![img_11.png](img_11.png)