package com.example.answer_system.mapper;

import com.example.answer_system.base.BasePlusMapper;
import com.example.answer_system.entity.SystemUser;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SystemUserMapper extends BasePlusMapper<SystemUser> {
    int deleteUserByIds(List<Long> ids);
}
