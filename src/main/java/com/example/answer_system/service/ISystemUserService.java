package com.example.answer_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.answer_system.entity.SystemUser;

import java.util.List;

public interface ISystemUserService extends IService<SystemUser> {
    /**
     * 用户登录
     * @param systemUser
     * @return
     */
    String userLogin(SystemUser systemUser);

    /**
     * 新增用户
     * @param systemUser
     * @return
     */
    String addUser(SystemUser systemUser);

    String updateUser(SystemUser systemUser);

    /**
     * 批量删除用户
     * @param list
     * @return
     */
    String removeUserByIds(List<Long> list);

    /**
     * 根据id获取用户信息
     * @param id
     * @return
     */
    SystemUser getUserById(Long id);
}
