package com.example.answer_system.service.impl;

import com.example.answer_system.config.MinioTemplate;
import com.example.answer_system.service.IMinioService;
import com.example.answer_system.utils.BusinessException;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;

@Service
public class MinioServiceImpl implements IMinioService {
    @Resource
    private MinioTemplate minioTemplate;

    @Override
    public String getFileUrl(String objectName){
        return minioTemplate.getObjectURL("",objectName,60 * 60 * 24);
    }

    @Override
    public String uploadFile(MultipartFile file){
        try {
            if (file.isEmpty()) {
                throw new BusinessException("上传文件不能为空");
            } else {
                // 得到文件流
                InputStream is = file.getInputStream();
                // 文件名
                String fileName = file.getOriginalFilename();
                String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
                // 把文件放到minio的boots桶里面
                minioTemplate.putObject("","pdfFile/"+fileName,is,-1,suffix);
                // 关闭输入流
                is.close();
                return "上传成功！";
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new BusinessException("上传失败");
        }
    }

    @Override
    public String removeFile(String filePath){
        try {
            minioTemplate.removeObject("",filePath);
            return "OK";
        }catch (Exception ex){
            ex.printStackTrace();
            throw new BusinessException("删除失败");
        }

    }

}
