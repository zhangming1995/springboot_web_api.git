package com.example.answer_system.service.impl;

import com.example.answer_system.service.IMailService;
import com.example.answer_system.utils.BusinessException;
import com.example.answer_system.utils.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;

@Service
public class MailServiceImpl implements IMailService {
    @Resource
    public JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sendUserMailName;

    @Resource
    private TemplateEngine templateEngine;

    //简单文本
    @Override
    public String sendMailQQMsg(){
        SimpleMailMessage message=new SimpleMailMessage();//构建一个邮件对象
        message.setSubject("使用springboot发送邮件测试");//设置邮件主题
        message.setFrom(sendUserMailName);//设置邮件发送人，要与application.yml文件配置一致
        message.setTo(sendUserMailName);//设置收件人,如果有多个接收人，使用","隔开
        //message.setCc("3121624188@qq.com");//设置抄送人，可以有多个
        //message.setBcc("3121624188@qq.com");//设置隐秘抄送人，可以有多个
        message.setSentDate(new Date());//设置发送日期
        message.setText("小程使用springboot发送邮件的简单测试！！");//设置邮件正文
        javaMailSender.send(message);//发送邮件
        return "OK";
    }

    @Override
    public String sendAttachFileMail(String filePath) {

        try {
            MimeMessage mimeMessage=javaMailSender.createMimeMessage();
            MimeMessageHelper helper=new MimeMessageHelper(mimeMessage,true);//true表示构建一个带附件的邮件对象
            helper.setSubject("这是一封测试邮件，带附件的");
            helper.setFrom(sendUserMailName);
            helper.setTo(new String[]{sendUserMailName});
            helper.setSentDate(new Date());
            helper.setText("小程使用springboot发送可以带附件的邮件 测试！！");
            helper.addAttachment("james.jpg", FileUtils.fileUrlConvertToMultipartFile(filePath));//第一个参数是自定义的名称，第二个参数是文件的位置
            helper.addAttachment("test.jpg", FileUtils.fileUrlConvertToMultipartFile(filePath));//第一个参数是自定义的名称，第二个参数是文件的位置
            javaMailSender.send(mimeMessage);
        }catch (MessagingException ex){
            throw new BusinessException("发送异常");
        }catch (Exception ex){
            ex.printStackTrace();
        }

        finally {
            return "OK";
        }
    }

    @Override
    public String sendThymeleaf(){
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setSubject("使用Thymeleaf模板作为发送邮件的模板");
            helper.setFrom(sendUserMailName);
            helper.setTo("17702206627@163.com");
            helper.setSentDate(new Date());
            //这里进入的是Template的Context
            Context context = new Context();
            //设置模板中的变量
            context.setVariable("username","程婷");
            context.setVariable("position","java开发工程师");
            context.setVariable("salary","XXXXXK");
            //第一个参数作为模板的，名称
            String process = templateEngine.process("email.html", context);
            //第二个参数true表示这是html文本
            helper.setText(process,true);
            javaMailSender.send(mimeMessage);
        }catch (MessagingException ex){
            throw new BusinessException("失败");
        }
        return "OK";
    }

}
