package com.example.answer_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.answer_system.entity.SystemUser;
import com.example.answer_system.mapper.SystemUserMapper;
import com.example.answer_system.service.ISystemUserService;
import com.example.answer_system.utils.BusinessException;
import com.example.answer_system.utils.JwtUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SystemUserServiceImpl extends ServiceImpl<SystemUserMapper,SystemUser> implements ISystemUserService {

    @Override
    public String userLogin(SystemUser systemUser){

        QueryWrapper<SystemUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_code",systemUser.getUserCode());
        List<SystemUser> list=baseMapper.selectList(wrapper);
        if(list.size()==0){
            throw new BusinessException("该用户不存在");
        }
        if(!systemUser.getPassword().equals(list.get(0).getPassword())){
            throw new BusinessException("密码不正确");
        }
        SystemUser user=list.get(0);
        //生成token
        return JwtUtil.createToken(user.getUserName(),user.getPassword(),user.getId(),user.getUserCode());
    }

    @Override
    @Transactional
    public String addUser(SystemUser systemUser){
        baseMapper.insert(systemUser);
        return "OK";
    }

    @Override
    @Transactional
    public String updateUser(SystemUser systemUser){
        baseMapper.updateById(systemUser);
        return "OK";
    }

    @Override
    @Transactional
    public String removeUserByIds(List<Long> list){
        baseMapper.deleteUserByIds(list);
        return "OK";
    }

    @Override
    public SystemUser getUserById(Long id){
        return baseMapper.selectById(id);
    }
}
