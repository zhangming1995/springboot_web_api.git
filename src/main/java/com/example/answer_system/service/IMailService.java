package com.example.answer_system.service;

//发送邮箱专用
public interface IMailService {
    //发送QQ邮件
    String sendMailQQMsg();
    //发送带附件的邮件
    String sendAttachFileMail(String filePath);
    //发送邮件模板
    String sendThymeleaf();
}
