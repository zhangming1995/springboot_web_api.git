package com.example.answer_system.service;

import org.springframework.web.multipart.MultipartFile;

public interface IMinioService {
    //获取文件预览地址
    String getFileUrl(String objectName);

    /**
     * 上传附件
     * @param file
     * @return
     */
    String uploadFile(MultipartFile file);

    /**
     * 删除文件
     * @param filePath
     * @return
     */
    String removeFile(String filePath) throws Exception;
}
