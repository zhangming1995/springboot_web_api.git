package com.example.answer_system.config;

import com.example.answer_system.utils.BusinessException;
import com.example.answer_system.utils.ResultMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
@Slf4j
public class ResponseWebExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    public ResultMap exceptionHandler(Exception e){
        log.error("",e);
        if (e instanceof DuplicateKeyException){
            return ResultMap.UNIQUE_PRIMARY_KEY.setNewErrorMsg("索引重复");
        }else if(e instanceof BusinessException){
            return ResultMap.BUSINESS_ERROR.setNewErrorMsg(e.getMessage());
        }else if(e instanceof MethodArgumentNotValidException){
            //参数验证捕捉
            BindingResult bindingResult = ((MethodArgumentNotValidException) e).getBindingResult();
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            StringBuilder errorMessage = new StringBuilder();
            for (FieldError fieldError : fieldErrors) {
                errorMessage.append(fieldError.getDefaultMessage()).append("！");
            }
            return ResultMap.OTHER_SYSTEM_ERROR.setNewErrorMsg(errorMessage.toString());
        } else {
            return ResultMap.OTHER_SYSTEM_ERROR.setNewErrorMsg(e.toString());
        }
    }
}
