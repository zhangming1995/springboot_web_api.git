package com.example.answer_system.config;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class PageResult {
    public Map<String,Object> GetPageResult(Object list, Long count){
        Map<String,Object> result=new HashMap<>();
        result.put("list",list);
        result.put("count",count);
        return result;
    }
}
