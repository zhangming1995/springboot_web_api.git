package com.example.answer_system.utils;

import lombok.Data;

@Data
public class ResultMap<T> {
    //错误码
    private Integer statusCode;

    //错误信息，一般为前端提示信息
    private String message;

    //返回值，一般为成功后返回的数据
    private T data;

    //错误详情，一般为失败后的详细原因，如空指针之类的
//    private String resultDetail;

    public ResultMap() {

    }

    public ResultMap(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public ResultMap(Integer statusCode, String message, T data) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    /**
     * 配合静态对象直接设置 data 参数
     */
    public ResultMap setNewData(T data) {
        ResultMap resultMap = new ResultMap();
        resultMap.setStatusCode(this.statusCode);
        resultMap.setMessage(this.message);
        resultMap.setData(data);
        return resultMap;
    }

    /**
     * 配合静态对象直接设置 errorMsg 参数
     */
    public ResultMap setNewErrorMsg(String message) {
        ResultMap resultMap = new ResultMap();
        resultMap.setStatusCode(this.statusCode);
        resultMap.setMessage(message);
        resultMap.setData(this.data);
        return resultMap;
    }

    public ResultMap setNewResult(Integer statusCode, String message, T data) {
        ResultMap resultMap = new ResultMap();
        resultMap.setStatusCode(statusCode);
        resultMap.setMessage(message);
        resultMap.setData(data);
        return resultMap;
    }

    public static final ResultMap SUCCESS = new ResultMap(1002, "ok");

    public static final ResultMap UNIQUE_PRIMARY_KEY = new ResultMap(1003, "唯一键异常");

    public static final ResultMap BUSINESS_ERROR=new ResultMap(1004, "");

    public static final ResultMap OTHER_SYSTEM_ERROR = new ResultMap(1005, "调用系统异常");

}
