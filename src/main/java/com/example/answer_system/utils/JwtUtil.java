package com.example.answer_system.utils;

import io.jsonwebtoken.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class JwtUtil {
    private static long time=1000*60*60*2;
    private static String signature="admin";
    public static String createToken(String userName,String userPassword,Long userId,String userCode){
        JwtBuilder jwtBuilder= Jwts.builder();
        String jwtToken=jwtBuilder
                //header
                .setHeaderParam("typ","JWT")
                .setHeaderParam("alg","HS256")
                //payload
                .claim("userName",userName)
                .claim("password",userPassword)
                .claim("userId",userId)
                .claim("userCode",userCode)
                .setSubject("admin")
                .setExpiration(new Date(System.currentTimeMillis()+time))
                .setId(UUID.randomUUID().toString())
                //signature
                .signWith(SignatureAlgorithm.HS256,signature)
                .compact();
        return jwtToken;
    }

    public static String verify(String token) {
        /**
         * @desc 验证token，通过返回true
         * @params [token]需要校验的串
         **/
        if(token==null || token==""){
            return "tokenIsNull";
        }
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(signature).parseClaimsJws(token);
        } catch (ExpiredJwtException ex) {
            throw new BusinessException("登录过期，请重新登录");
        }
        return "success";
    }

    /**
     * 接口解析token
     * @param token
     * @return
     */
    public static Map<String, Object> getTokenInform(String token){
        JwtParser jwtParser=Jwts.parser();
        Jws<Claims> claimsJws=jwtParser.setSigningKey(signature).parseClaimsJws(token);
        Claims claims=claimsJws.getBody();
        Map<String,Object> result=new HashMap<>();
        result.put("userName",claims.get("userName"));
        result.put("userId",claims.get("userId"));
        result.put("userCode",claims.get("userCode"));
        return result;
    }
}
