package com.example.answer_system.utils;

public class BusinessException extends RuntimeException {
    public BusinessException(String msg){
        super(msg);
    }
}
