package com.example.answer_system.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.example.answer_system.base.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@TableName("system_user")
public class SystemUser extends BaseEntity {

  @NotNull(message = "用户名不能为空")//空校验
  @NotBlank(message = "用户名不能为空") //不能为空字符串
  private String userName;

  private String nickName;

  @NotNull(message = "用户编码不能为空")//空校验
  @NotBlank(message = "用户编码不能为空") //不能为空字符串
  private String userCode;

  @NotNull(message = "密码不能为空")//空校验
  @NotBlank(message = "密码不能为空")  //不能为空字符串
  @Length(max = 6,message = "密码不得超过6位")
  private String password;

  @NotNull(message = "类型不能为空")//空校验
  @NotBlank(message = "类型不能为空")//不能为空字符串
  private String type;


}
