package com.example.answer_system.controller;

import com.example.answer_system.service.IMinioService;
import com.example.answer_system.utils.BusinessException;
import com.example.answer_system.utils.ResultMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@RequestMapping("/minio")
public class MinioController {
    @Resource
    private IMinioService minioService;

    @GetMapping("/getFileUrl")
    public ResultMap getFileUrl(@RequestParam("filePath") String filePath){
        return ResultMap.SUCCESS.setNewData(minioService.getFileUrl(filePath));
    }

    /**
     * 上传附件
     * @param file
     * @return
     */
    @PostMapping("/uploadFile")
    public ResultMap uploadFile(@RequestParam("file") MultipartFile file){
        return ResultMap.SUCCESS.setNewData(minioService.uploadFile(file));
    }

    @GetMapping("/deleteFile")
    public ResultMap deleteFile(@RequestParam("filePath") String filePath) {
        try {
            return ResultMap.SUCCESS.setNewData(minioService.removeFile(filePath));
        }catch (Exception ex){
            ex.printStackTrace();
            throw new BusinessException("删除失败");
        }

    }
}
