package com.example.answer_system.controller;

import com.example.answer_system.entity.SystemUser;
import com.example.answer_system.service.IMailService;
import com.example.answer_system.utils.ResultMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class MailController {
    @Resource
    private IMailService mailService;

    @GetMapping("/sendMailText")
    public ResultMap sendMailText(){
        return ResultMap.SUCCESS.setNewData(mailService.sendMailQQMsg());
    }

    @PostMapping("/sendAttachFileMail")
    public ResultMap sendAttachFileMail(@RequestBody Map<String,String> map){
        return ResultMap.SUCCESS.setNewData(mailService.sendAttachFileMail(map.get("filePath")));
    }

    @GetMapping("/sendThymeleaf")
    public ResultMap sendThymeleaf(){
        return ResultMap.SUCCESS.setNewData(mailService.sendThymeleaf());
    }
}
