package com.example.answer_system.controller;

import com.example.answer_system.entity.SystemUser;
import com.example.answer_system.service.ISystemUserService;
import com.example.answer_system.utils.BusinessException;
import com.example.answer_system.utils.ResultMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/user")
public class SystemUserController {
    @Resource
    private ISystemUserService systemUserService;

    @PostMapping("/login")
    public ResultMap login(@RequestBody SystemUser systemUser){
        return ResultMap.SUCCESS.setNewData(systemUserService.userLogin(systemUser));
    }

    @PostMapping("/insertUser")
    public ResultMap insertUser(@Validated @RequestBody SystemUser systemUser){
        return ResultMap.SUCCESS.setNewData(systemUserService.addUser(systemUser));
    }

    @PutMapping("/updateUser")
    public ResultMap updateUser(@Validated @RequestBody SystemUser systemUser){
        return ResultMap.SUCCESS.setNewData(systemUserService.updateUser(systemUser));
    }

    @DeleteMapping("/deleteUserByIds")
    public ResultMap deleteUserByIds(@RequestBody List<Long> ids){
        return ResultMap.SUCCESS.setNewData(systemUserService.removeUserByIds(ids));
    }

    @GetMapping("/getUserById/{id}")
    public ResultMap<SystemUser> getUserById(@PathVariable("id") Long id){
        return ResultMap.SUCCESS.setNewData(systemUserService.getUserById(id));
    }
}
