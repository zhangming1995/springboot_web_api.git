package com.example.answer_system.base;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 实体类集成类
 */
@Data
public class BaseEntity {
    @TableId(type = IdType.AUTO)
    private Long id;

    //逻辑删除字段
    @TableLogic //逻辑删除注解
    private Integer isDelete;

    //字段添加填充内容
    //策略
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    //策略
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT)
    private Long createBy;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateBy;
}
