package com.example.answer_system.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Collection;

public interface BasePlusMapper <T> extends BaseMapper<T> {
    //真正的批量插入
    Integer insertBatchSomeColumn(Collection<T> entityList);
}
