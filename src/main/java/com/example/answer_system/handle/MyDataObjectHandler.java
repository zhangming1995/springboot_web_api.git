package com.example.answer_system.handle;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.answer_system.base.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class MyDataObjectHandler implements MetaObjectHandler {
    //插入时的填充策略
    @Override
    public void insertFill(MetaObject metaObject) {
        //setFieldValByName(String fieldName, Object fieldVal, MetaObject metaObject)
        this.setFieldValByName("createTime", LocalDateTime.now(),metaObject);
        this.setFieldValByName("updateTime",LocalDateTime.now(),metaObject);
        this.setFieldValByName("createBy", BaseContext.getUserId(),metaObject);
        this.setFieldValByName("updateBy", BaseContext.getUserId(),metaObject);
        this.setFieldValByName("isDelete", 0,metaObject);
    }
    //更新时的填充策略
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime",LocalDateTime.now(),metaObject);
        this.setFieldValByName("updateBy", BaseContext.getUserId(),metaObject);
    }
}
