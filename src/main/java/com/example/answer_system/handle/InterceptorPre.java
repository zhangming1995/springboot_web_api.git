package com.example.answer_system.handle;

import com.example.answer_system.base.BaseContext;
import com.example.answer_system.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class InterceptorPre implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler)
            throws Exception {
        String token=request.getHeader("token");
        String verifyToken= JwtUtil.verify(token);
        if(verifyToken=="success"){
            Long userId=Long.valueOf(JwtUtil.getTokenInform(token).get("userId").toString());
            BaseContext.setUserId(userId);
            return true;
        } else if(verifyToken=="tokenIsNull"){
            throw new Exception("noToken");
        } else {
            return false;
        }
        //System.out.println("处理器前方法");
        // 返回true，不会拦截后续的处理

    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // System.out.println("处理器后方法");
        log.info("处理后");
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        BaseContext.remove();
    }
}
